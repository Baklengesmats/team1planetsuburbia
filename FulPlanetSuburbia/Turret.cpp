#include "Turret.h"

using namespace std;

Turret::Turret()
{
	spriteOver.setPosition(0, 0);
	spriteOver.setTextureRect(sf::IntRect(0, 0, 60, 60));
	spriteOver.setOrigin(30, 30);
	spriteNeder.setPosition(0, 0);
	spriteNeder.setTextureRect(sf::IntRect(0, 0, 60, 60));
	newTurnCounterTM = 2;
	newTurnTM = true;
}

void Turret::movement(bool turn)
{
	if (turn == true)
	{
		spriteOver.rotate(25);
	}
	else if (turn == false)
	{
		spriteOver.rotate(-25);
	}
}

float Turret::getRotationTM()
{
	return newRotationTM;
}

void Turret::setRotationTM(float rotationTM)
{
	newRotationTM = rotationTM;
}

int Turret::getTurnCounterTM()
{
	return newTurnCounterTM;
}
bool Turret::getTurnTM()
{
	return newTurnTM;
}

void Turret::setTurnCounterTM(int counterTM)
{
	newTurnCounterTM = counterTM;
}
void Turret::setTurnTM(bool TurnTM)
{
	newTurnTM = TurnTM;
}