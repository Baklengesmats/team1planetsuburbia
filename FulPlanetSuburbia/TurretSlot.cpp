#include "TurretSlot.h"


TurretSlot::TurretSlot(sf::RenderWindow &window)
{
	rect.setSize(sf::Vector2f(60, 60));
	rect.setFillColor(sf::Color::Red);
	rect.setPosition(0, 850);
	newFilled = false;

}

bool TurretSlot::getFilled()
{
	return newFilled;
}

void TurretSlot::setFilled(bool isFilled)
{
	newFilled = isFilled;
}