#pragma once

#include "Incl.h"

using namespace std;

class MenuButton
{
public:

	sf::RectangleShape rect;
	sf::Sprite sprite;
	int Bnr;

	MenuButton();

	int getNumber();
	void setNumber(int number);
	bool getActive();
	void setActive(bool active);

private:
	int newNumber;
	bool newActive;
};

