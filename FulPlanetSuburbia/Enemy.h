#pragma once

#include "Incl.h"

class enemy
{
public:
	enemy();
	~enemy();
	double movementSpeed = 1;
	int movementLength = 100;
	int attackDamage = 5;
	int counterWalking = 0;
	int direction = 0; // 1 - up, 2 - down, 3 - left, 4 - right
	int counter = 0;
	int hp = 5;
	bool alive = true;
	int destroyed = 0;
	int enemyLifeTime = 1000;
	sf::RectangleShape enemyrect;
	sf::Sprite enemysprite;

	int GenerateRandom(int max);



	void update();
	void updateMovement();
};