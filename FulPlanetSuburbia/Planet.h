#pragma once

#include "Incl.h"

class Planet
{
public:
	Planet();

	sf::RectangleShape rectOver1;
	sf::RectangleShape rectNeder1;

	sf::RectangleShape rectOver2;
	sf::RectangleShape rectNeder2;

	sf::RectangleShape rectOver3;
	sf::RectangleShape rectNeder3;

	sf::RectangleShape rectOver4;
	sf::RectangleShape rectNeder4;

	sf::RectangleShape rectSektor1;
	sf::RectangleShape rectSektor2;

	int getMarketValue();
	void setMarketValue(int marketvalue);

private:
	int newMarketValue;

};

