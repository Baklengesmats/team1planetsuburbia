#include "Rocket.h"

Rocket::Rocket(sf::RenderWindow &window)
{
	rectRocket.setSize(sf::Vector2f(20, 60));
	rectRocket.setFillColor(sf::Color::Green);
	mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
}

void Rocket::movementFromAvatar(sf::RenderWindow &window, Avatar avatar, Planet planet)
{
	rectRocket.setRotation(rotation);
	spriteRocket.setRotation(rotation);
	spriteRocket.setPosition(rectRocket.getPosition());

	angle = atan2((mousePos.y - rectRocket.getGlobalBounds().height / 2) - (avatar.spriteOver.getPosition().y - 60), // sektor2 = 1860 // sektor1 = 860
		(mousePos.x - rectRocket.getGlobalBounds().width / 2) - (avatar.spriteOver.getPosition().x - 15)); //sektor2 = 1521 // sektor1 = 497
	rectRocket.move(cos(angle)*7, sin(angle)*7);

	if (planet.rectSektor1.getGlobalBounds().contains(rectRocket.getPosition()) == false
		&& planet.rectSektor2.getGlobalBounds().contains(rectRocket.getPosition()) == false)
	{
		destroy = true;
	}

}