#include "Incl.h"
#include "Avatar.h"
#include "Rocket.h"
#include "Turret.h"
#include "TurretSlot.h"
#include "EMP.h"
#include "MenuButton.h"
#include "Enemy.h"
#include "Planet.h"

using namespace std;

int main()
{
	// Declarations for Variables
	enum Status { STARTSCREEN, MAINMENU, PLAY, OPTIONS, PAUSED, QUIT, CONTROLS, ENDSCREEN };

	bool BgStars = true;
	bool TSActive = true;
	bool firstRocketDelay = true;
	bool pressedL = true;
	bool pressedL1 = true;

	int counterTM = 0;
	int counterTS = 0;
	int counterT = 0;
	int counterCTR = 0;
	int counterAR = 0;
	int counterTR = 0;
	int counterMB = 0;
	int counterE = 0;
	int counterPE = 0;
	int counterPE2 = 0;
	int counterEMP = 0;
	int scoreInt = 0;
	int marketValueInt = 0;
	int frameTimer = 0;


	sf::Vector2i rocketOrigin(502, 880);


	//HUD
	sf::Font font;
	sf::Text score;
	sf::Text marketValue;
	string scoreString;
	string marketValueString;

	//Views
	sf::Keyboard key;
	sf::View view[4];
	view[0].setCenter(512, 500);
	view[0].setSize(1024, 1000);

	view[1].setCenter(1536, 1500);
	view[1].setSize(1024, 1000);

	view[2].setCenter(2048, 2000);
	view[2].setSize(1024, 1000);

	view[3].setCenter(2560, 2500);
	view[3].setSize(1024, 1000);

	//text parameters set here
	score.setPosition(5, 10);
	score.setFont(font);
	score.setCharacterSize(20);
	score.setColor(sf::Color::Red);

	marketValue.setPosition(700, 10);
	marketValue.setFont(font);
	marketValue.setCharacterSize(20);
	marketValue.setColor(sf::Color::Red);

	sf::Clock clock;
	sf::Clock clockBG;
	sf::Clock clockCRA;
	sf::Clock clockCRT;
	sf::Clock clockTM;
	sf::Clock clockM;
	sf::Clock clockEnemy;
	sf::Clock clockEMP;

	sf::Time time = clock.getElapsedTime();
	sf::Time timerestart = clock.restart();

	sf::Time elapsedCRA;

	float timer = 0;
	float coolDownTimer = 0;

	sf::RenderWindow window(sf::VideoMode(1024, 1000), "Planet Suburbia!", sf::Style::Titlebar | sf::Style::Close);

	window.setFramerateLimit(60);

	// Load font 
	if (!font.loadFromFile("../Textures/CandyShop.ttf"))
	{
		cout << "Error! Couldn't load font" << endl;
	}
	// Load Textures

	sf::Texture T_AvatarNeder;
	if (!T_AvatarNeder.loadFromFile("../Textures/turret plattform v2.png", sf::IntRect(0, 0, 120, 120)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_AvatarOver;
	if (!T_AvatarOver.loadFromFile("../Textures/turret �verdel v2.png", sf::IntRect(0, 0, 75, 150)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Background;
	if (!T_Background.loadFromFile("../Textures/Background.png", sf::IntRect(0, 0, 1024, 1000)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_BackgroundGlowing;
	if (!T_BackgroundGlowing.loadFromFile("../Textures/BackgroundGlowing.png", sf::IntRect(0, 0, 1024, 1000)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Sektor2;
	if (!T_Sektor2.loadFromFile("../Textures/Sektor 4.png", sf::IntRect(0, 0, 1024, 1000)))
	{
		cout << "Error! Couldn't load AI_Turret_Nederdel.png" << endl;
	}
	sf::Texture T_Rocket;
	if (!T_Rocket.loadFromFile("../Textures/Rocket.png", sf::IntRect(0, 0, 20, 60)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_AITurretOver;
	if (!T_AITurretOver.loadFromFile("../Textures/AI_Turret_Overdel.png", sf::IntRect(0, 0, 60, 60)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_AITurretNeder;
	if (!T_AITurretNeder.loadFromFile("../Textures/AI_Turret_Nederdel.png", sf::IntRect(0, 0, 60, 60)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_StartScreen;
	if (!T_StartScreen.loadFromFile("../Textures/Start1.png", sf::IntRect(0, 0, 1024, 1000)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Play;
	if (!T_Play.loadFromFile("../Textures/GUI Play.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Options;
	if (!T_Options.loadFromFile("../Textures/GUI Options.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Quit;
	if (!T_Quit.loadFromFile("../Textures/GUI Quit.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Controls;
	if (!T_Controls.loadFromFile("../Textures/GUI Controls.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_ControlsGuide;
	if (!T_ControlsGuide.loadFromFile("../Textures/GUI Controls Guide.png", sf::IntRect(0, 0, 400, 600)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Highscore;
	if (!T_Highscore.loadFromFile("../Textures/GUI Highscore.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Enemy;
	if (!T_Enemy.loadFromFile("../Textures/Alienskepp1.png", sf::IntRect(0, 0, 50, 30)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Enemy2;
	if (!T_Enemy2.loadFromFile("../Textures/BigBoss.png", sf::IntRect(0, 0, 80, 60)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_Back;
	if (!T_Back.loadFromFile("../Textures/GUI BACK.png", sf::IntRect(0, 0, 300, 100)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}
	sf::Texture T_EMP;
	if (!T_EMP.loadFromFile("EMP.png", sf::IntRect(0, 0, 2038, 1813)))
	{
		cout << "Error! Couldn't load AI_Turret_Nederdel.png" << endl;
	}


	// Create Objects, Vectors and set Textures to Sprites

	Status gameStatus = MAINMENU;

	sf::Sprite S_StartScreen;
	S_StartScreen.setTexture(T_StartScreen);
	sf::Sprite S_Background;
	S_Background.setTexture(T_Background);
	sf::Sprite S_ControlsGuide;
	S_ControlsGuide.setTexture(T_ControlsGuide);
	S_ControlsGuide.setPosition(312, 150);

	Planet planet;
	planet.setMarketValue(1000);
	vector<sf::RectangleShape>::const_iterator iterP;
	vector<sf::RectangleShape> planetRectArray;
	planetRectArray.push_back(planet.rectNeder1);
	planetRectArray.push_back(planet.rectOver1);
	planetRectArray.push_back(planet.rectNeder2);
	planetRectArray.push_back(planet.rectOver2);

	EMP emp;
	emp.spriteEMP.setTexture(T_EMP);

	Avatar avatar;
	avatar.spriteNeder.setTexture(T_AvatarNeder);
	avatar.spriteOver.setTexture(T_AvatarOver);

	enemy enemy1;
	enemy1.enemysprite.setTexture(T_Enemy);
	sf::Sprite Boss01;
	Boss01.setTexture(T_Enemy2);
	Boss01.setPosition(400, 400);
	sf::RectangleShape rectboss;
	rectboss.setFillColor(sf::Color::Green);
	rectboss.setSize(sf::Vector2f(80, 60));

	vector<enemy>::const_iterator iter4;
	vector<enemy> enemyArray;
	enemyArray.push_back(enemy1);

	vector<Rocket>::const_iterator iterAR;
	vector<Rocket> RocketArrayAR;
	Rocket rocketAR(window);
	rocketAR.spriteRocket.setTexture(T_Rocket);

	vector<Rocket>::const_iterator iterTR;
	vector<Rocket> RocketArrayTR;
	Rocket rocketTR(window);
	rocketTR.spriteRocket.setTexture(T_Rocket);

	vector<Turret>::const_iterator iterT;
	vector<Turret> TurretArray;
	Turret turret;
	turret.spriteNeder.setTexture(T_AITurretNeder);
	turret.spriteOver.setTexture(T_AITurretOver);

	vector<TurretSlot>::const_iterator iterTS;
	vector<TurretSlot> TurretSlotArray;
	TurretSlot turretSlot(window);
	float avsTS = 115; // 103
	//turretslots sector 1
	for (int i = 0; i < 6; i++)
	{
		turretSlot.rect.setPosition(avsTS, 900);
		if (avsTS >= 300 && avsTS <= 400)
			avsTS = 643;
		else
			avsTS += 103;
		TurretSlotArray.push_back(turretSlot);
	}
	avsTS = 1139;
	//turretslots sector 2
	for (int i = 0; i < 6; i++)
	{
		turretSlot.rect.setPosition(avsTS, 1900);
		if (avsTS >= 1324 && avsTS <= 1424)
			avsTS = 1667;
		else
			avsTS += 103;
		TurretSlotArray.push_back(turretSlot);
	}

	vector<MenuButton>::const_iterator iterMMB;
	vector<MenuButton> MainMenuButtonArray;
	vector<MenuButton>::const_iterator iterOB;
	vector<MenuButton> OptionsButtonArray;
	MenuButton playB;
	playB.sprite.setTexture(T_Play);
	playB.rect.setPosition(362, 350); // 5 st 250-650
	playB.Bnr = 1;
	playB.setNumber(1);
	MainMenuButtonArray.push_back(playB);
	MenuButton optionsB;
	optionsB.sprite.setTexture(T_Options);
	optionsB.rect.setPosition(362, 450);
	optionsB.Bnr = 2;
	optionsB.setNumber(2);
	MainMenuButtonArray.push_back(optionsB);
	MenuButton quitB;
	quitB.sprite.setTexture(T_Quit);
	quitB.rect.setPosition(362, 550);
	quitB.Bnr = 3;
	quitB.setNumber(3);
	MainMenuButtonArray.push_back(quitB);
	MenuButton controlsB;
	controlsB.sprite.setTexture(T_Controls);
	controlsB.rect.setPosition(362, 350);
	controlsB.Bnr = 4;
	controlsB.setNumber(4);
	OptionsButtonArray.push_back(controlsB);
	MenuButton highscoreB;
	highscoreB.sprite.setTexture(T_Highscore);
	highscoreB.rect.setPosition(362, 450);
	highscoreB.Bnr = 5;
	highscoreB.setNumber(5);
	OptionsButtonArray.push_back(highscoreB);
	MenuButton backB;
	backB.sprite.setTexture(T_Back);
	backB.rect.setPosition(362, 550);
	backB.Bnr = 6;
	backB.setNumber(6);
	OptionsButtonArray.push_back(backB);

	// Window.isOpen()

	const float targettime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	while (window.isOpen())
	{
		timer++;
		sf::Time deltatime = clock.restart();
		frametime = std::min(deltatime.asSeconds(), 0.1f);
		if (frametime > 0.1f)
			frametime = 0.1f;
		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;

			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window.close();
				}
				if (event.type == sf::Event::MouseMoved)
				{
					if (avatar.getRotation(window) < 90)
						avatar.LookMouse(window);
				}

			}
		}

		// Window Clear
		window.clear(sf::Color(0x011, 0x52, 0x33, 0xff));
		// GUI
		// Start Screen
			// GUI
		if (gameStatus == STARTSCREEN)
		{
			window.draw(S_StartScreen);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
			{
				gameStatus = MAINMENU;
			}
		}
		else if (gameStatus == MAINMENU)
		{
			window.draw(S_StartScreen);
			counterMB = 0;
			for (iterMMB = MainMenuButtonArray.begin(); iterMMB < MainMenuButtonArray.end(); iterMMB++)
			{

				MainMenuButtonArray[counterMB].sprite.setPosition(MainMenuButtonArray[counterMB].rect.getPosition());
				window.draw(MainMenuButtonArray[counterMB].sprite);

				sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
				if (MainMenuButtonArray[counterMB].rect.getGlobalBounds().contains(mousePos)) {
					window.draw(MainMenuButtonArray[counterMB].rect);
					if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && pressedL)
					{
						pressedL = false;
						if (MainMenuButtonArray[counterMB].getNumber() == 1)
							gameStatus = PLAY;
						if (MainMenuButtonArray[counterMB].getNumber() == 2)
							gameStatus = OPTIONS;
						if (MainMenuButtonArray[counterMB].getNumber() == 3)
							gameStatus = QUIT;
					}
					if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !pressedL)
						pressedL = true;
				}
				counterMB++;
			}
		}
		else if (gameStatus == OPTIONS)
		{
			window.draw(S_StartScreen);
			counterMB = 0;

			for (iterOB = OptionsButtonArray.begin(); iterOB < OptionsButtonArray.end(); iterOB++)
			{

				OptionsButtonArray[counterMB].sprite.setPosition(OptionsButtonArray[counterMB].rect.getPosition());
				window.draw(OptionsButtonArray[counterMB].sprite);

				sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
				if (OptionsButtonArray[counterMB].rect.getGlobalBounds().contains(mousePos)) {
					window.draw(OptionsButtonArray[counterMB].rect);
					if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && pressedL)
					{
						pressedL = false;
						if (OptionsButtonArray[counterMB].getNumber() == 4)
							gameStatus = CONTROLS;
						if (OptionsButtonArray[counterMB].getNumber() == 5)
							gameStatus = OPTIONS;
						if (OptionsButtonArray[counterMB].getNumber() == 6)
							gameStatus = MAINMENU;
					}
					if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !pressedL)
						pressedL = true;
				}
				counterMB++;
			}
		}
		else if (gameStatus == CONTROLS)
		{
			window.draw(S_StartScreen);
			window.draw(S_ControlsGuide);
			backB.rect.setPosition(362, 750);
			backB.sprite.setPosition(backB.rect.getPosition());
			window.draw(backB.sprite);
			sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
			if (playB.rect.getGlobalBounds().contains(mousePos)) {
				window.draw(backB.rect);
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && pressedL)
				{
					pressedL = false;
					gameStatus = OPTIONS;
				}
				if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !pressedL)
					pressedL = true;
			}
		}

		else if (gameStatus == PAUSED || gameStatus == ENDSCREEN)
		{
			window.draw(S_Background);
			window.draw(Boss01);

			window.draw(planet.rectNeder2);
			window.draw(planet.rectOver2);
			/*window.draw(planet.rectSektor1);
			window.draw(planet.rectSektor2);*/

			counterE = 0;
			for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
			{
				window.draw(enemyArray[counterE].enemysprite);
				counterE++;
			}
			counterT = 0;
			for (iterT = TurretArray.begin(); iterT < TurretArray.end(); iterT++) {

				window.draw(TurretArray[counterT].spriteOver);
				window.draw(TurretArray[counterT].spriteNeder);
				counterT++;
			}
			counterAR = 0;
			for (iterAR = RocketArrayAR.begin(); iterAR != RocketArrayAR.end(); iterAR++)
			{
				window.draw(RocketArrayAR[counterAR].spriteRocket);
				counterAR++;
			}
			counterTR = 0;
			for (iterTR = RocketArrayTR.begin(); iterTR != RocketArrayTR.end(); iterTR++)
			{
				window.draw(RocketArrayTR[counterTR].spriteRocket);
				counterTR++;
			}
			window.draw(avatar.spriteNeder);
			window.draw(avatar.spriteOver);


			if (gameStatus == ENDSCREEN) {
				cout << "GAME LOST, your marketvalue is 0" << endl;
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
					gameStatus = MAINMENU;

					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && pressedL)
					{

						gameStatus = MAINMENU;
						pressedL = false;
					}
					if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && !pressedL)
						pressedL = true;
				}

				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && pressedL)
				{

					gameStatus = PLAY;
					pressedL = false;
				}
				if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && !pressedL)
					pressedL = true;
			}

			else if (gameStatus == QUIT)
			{
				window.close();
			}

			else if (gameStatus == PLAY)
			{

				//Swap Sectors

				if (key.isKeyPressed(sf::Keyboard::Num1))
				{
					S_Background.setTexture(T_Background);
					window.setView(view[0]);
					score.setPosition(5, 10);
					marketValue.setPosition(700, 10);
					S_Background.setPosition(0, 0);
					avatar.spriteOver.setPosition(512, 920);
					avatar.spriteNeder.setPosition(452, 860);
					//rocketOrigin = sf::Vector2i(502, 880);
				}
				if (key.isKeyPressed(sf::Keyboard::Num2))
				{
					S_Background.setTexture(T_Sektor2);
					window.setView(view[1]);
					score.setPosition(1029, 1010);
					marketValue.setPosition(1724, 1010);
					S_Background.setPosition(1024, 1000);
					avatar.spriteOver.setPosition(1536, 1920);
					avatar.spriteNeder.setPosition(1476, 1860);
					//rocketOrigin = sf::Vector2i(1526, 1880);
				}
				/*if (key.isKeyPressed(sf::Keyboard::Num3))
				{
					window.setView(view[2]);
					score.setPosition(1541, 1510);
					marketValue.setPosition(2236, 1510);
				}
				if (key.isKeyPressed(sf::Keyboard::Num4))
				{
					window.setView(view[3]);
					score.setPosition(2053, 2010);
					marketValue.setPosition(2748, 2010);
				}*/

				// Background
				/*sf::Time elapsedBG = clockBG.getElapsedTime();
				if (elapsedBG.asMilliseconds() >= 800)
				{
					clockBG.restart();
					if (BgStars == true) {
						S_Background.setTexture(T_Background);
						BgStars = false;
					}
					else if (BgStars == false) {
						S_Background.setTexture(T_BackgroundGlowing);
						BgStars = true;
					}
				}*/

				//timer for score
				if (frameTimer >= 60)
				{
					scoreInt++;
					frameTimer = 0;
				}
				frameTimer++;
				scoreString = "Score :" + to_string(scoreInt);
				score.setString(scoreString);
				marketValueString = "Marketvalue :" + to_string(planet.getMarketValue());
				marketValue.setString(marketValueString);
				window.draw(S_Background);
				window.draw(score);
				window.draw(marketValue);

				// Cap Marketvalue
				if (planet.getMarketValue() >= 1500)
					planet.setMarketValue(1500);

				// Handle Turretslots
				/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
				{
					TSActive = true;
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					TSActive = false;
				}*/

				sf::Vector2f mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
				counterTS = 0;
				for (iterTS = TurretSlotArray.begin(); iterTS != TurretSlotArray.end(); iterTS++)
				{
					if (TSActive == true) {
						if (TurretSlotArray[counterTS].rect.getGlobalBounds().contains(mousePos))
						{
							if (TurretSlotArray[counterTS].getFilled() == false)
							{
								TurretSlotArray[counterTS].rect.setFillColor(sf::Color::Green);
							}
							else TurretSlotArray[counterTS].rect.setFillColor(sf::Color::Blue);
							if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && TurretSlotArray[counterTS].getFilled() == false) {
								if (planet.getMarketValue() > 500)
								{
									turret.spriteNeder.setPosition(TurretSlotArray[counterTS].rect.getPosition());
									turret.spriteOver.setPosition(TurretSlotArray[counterTS].rect.getPosition().x + 30, TurretSlotArray[counterTS].rect.getPosition().y + 30);
									TurretArray.push_back(turret);
									planet.setMarketValue(planet.getMarketValue() - 500);
									TurretSlotArray[counterTS].setFilled(true);
								}
							}
						}
						else if (TurretSlotArray[counterTS].getFilled() == true) {
							TurretSlotArray[counterTS].rect.setFillColor(sf::Color::Blue);
						}
						else TurretSlotArray[counterTS].rect.setFillColor(sf::Color::Red);
						window.draw(TurretSlotArray[counterTS].rect);
						counterTS++;
					}
				}
				// cout << planet.getMarketValue() << endl;
				//Turret Movement
				counterTM = 0;
				sf::Time elapsedTM = clockTM.getElapsedTime();
				if (elapsedTM.asSeconds() >= 3)
				{
					clockTM.restart();
					for (iterT = TurretArray.begin(); iterT < TurretArray.end(); iterT++) {

						if (TurretArray[counterTM].getTurnCounterTM() == 3) // if (turnCounter == 3)
							TurretArray[counterTM].setTurnTM(false); // turn = false;
						else if (TurretArray[counterTM].getTurnCounterTM() == 5) // else if (turnCounter == 5)
						{
							TurretArray[counterTM].setTurnCounterTM(1); // turnCounter = 1;
							TurretArray[counterTM].setTurnTM(true); // turn = true;
						}
						TurretArray[counterTM].movement(TurretArray[counterTM].getTurnTM()); // TurretArray[counterTM].movement(turn);
						TurretArray[counterTM].setTurnCounterTM(TurretArray[counterTM].getTurnCounterTM() + 1); // turnCounter++;

						counterTM++;
					}

				}

				// Create Rocket for Avatar
				//elapsedCRA = clockCRA.getElapsedTime();
				//if (elapsedCRA.asMilliseconds() >= 200)
				//{
				/*if (coolDownTimer >= 60)
				{*/
				if (coolDownTimer >= 0.5)
				{
					if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && pressedL1 && avatar.getRotation(window) < 90)

					{
						pressedL1 = false;
						rocketAR.rectRocket.setPosition(avatar.spriteOver.getPosition().x - 10, avatar.spriteOver.getPosition().y - 60); // sektor2 = 1526, 1880 // sektor1 = 502, 880 //Rocketsize/2.x - 512 , RocketSize/2.y - 920 Proj1 = (483,885)
						rocketAR.rotation = avatar.getRotation(window);
						rocketAR.mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
						RocketArrayAR.push_back(rocketAR);
					}
					if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !pressedL1)
						pressedL1 = true;
					coolDownTimer = 0;
				}
				coolDownTimer += targettime;

				// Create Rocket for Turrets
				counterCTR = 0;
				sf::Time elapsedCRT = clockCRT.getElapsedTime();
				for (iterT = TurretArray.begin(); iterT != TurretArray.end(); iterT++)
				{
					if (elapsedCRT.asSeconds() >= 3)
						//	clockCRA.restart();
						//}
						coolDownTimer++;
					/*	}*/

						// Create Rocket for Turrets
					counterCTR = 0;
					sf::Time elapsedCRT = clockCRT.getElapsedTime();
					for (iterT = TurretArray.begin(); iterT != TurretArray.end(); iterT++)
					{
						if (elapsedCRT.asSeconds() >= 3)
						{
							clockCRT.restart();
							rocketTR.rectRocket.setPosition(TurretArray[counterCTR].spriteNeder.getPosition());
							rocketTR.rotation = avatar.getRotation(window);
							rocketTR.mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
							//TurretArray[counterCTR].spriteOver.getRotation() + 180;
							RocketArrayTR.push_back(rocketTR);
						}
						counterCTR++;
					}

					//Draw Avatar Spirte nederdel
					window.draw(avatar.spriteNeder);

					// Draw and Move Rockets
					counterAR = 0;

					for (iterAR = RocketArrayAR.begin(); iterAR != RocketArrayAR.end(); iterAR++)
					{
						RocketArrayAR[counterAR].movementFromAvatar(window, avatar, planet);
						//window.draw(RocketArrayAR[counterAR].rectRocket);
						window.draw(RocketArrayAR[counterAR].spriteRocket);
						counterAR++;
					}
					counterTR = 0;
					for (iterTR = RocketArrayTR.begin(); iterTR != RocketArrayTR.end(); iterTR++)
					{
						RocketArrayTR[counterTR].movementFromAvatar(window, avatar, planet);
						window.draw(RocketArrayTR[counterTR].spriteRocket);
						counterTR++;
					}

					//Spawn enemies
					if (timer > 120)
					{
						enemy1.enemyrect.setPosition(enemy1.GenerateRandom(window.getSize().x - 45), 0);
						enemyArray.push_back(enemy1);
						if (scoreInt > 100)
						{
							enemy1.enemyrect.setPosition(enemy1.GenerateRandom(view[1].getCenter().x - -512 - 45), 1000);
							enemyArray.push_back(enemy1);
						}
						timer = 0;

					}
					//Enemy move
					counterE = 0;
					for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
					{
						enemyArray[counterE].update();
						enemyArray[counterE].updateMovement();
						//window.draw(enemyArray[counter].rect);
						window.draw(enemyArray[counterE].enemysprite);
						counterE++;
					}

					// EMP
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::C) && emp.getObtainedEMP() == true)
					{
						emp.setActivateEMP(true);
					}
					sf::Time elapsedEMP = clockEMP.getElapsedTime();
					if (elapsedEMP.asMilliseconds() >= 6)
					{
						if (emp.getActivateEMP() == true) {
							clockEMP.restart();
							emp.Activate(window);
							if (emp.getCR() == 400)
							{
								emp.setActivateEMP(false);
								emp.setCR(5);
								emp.setCX(498);
								emp.setCY(298);
								enemyArray.clear();
								emp.setObtainedEMP(false);
							}
						}
					}
					else if (elapsedEMP.asMilliseconds() < 6 && emp.getActivateEMP() == true)
					{
						window.draw(emp.circleEMP);
						//window.draw(emp.spriteEMP);
					}


					//collision procjetile and enemy
					counterPE = 0;
					for (iterAR = RocketArrayAR.begin(); iterAR != RocketArrayAR.end(); iterAR++)
					{
						counterPE2 = 0;
						for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
						{
							if (RocketArrayAR[counterPE].rectRocket.getGlobalBounds().intersects(enemyArray[counterPE2].enemyrect.getGlobalBounds()))
							{
								RocketArrayAR[counterPE].destroy = true;

								enemyArray[counterPE2].hp -= RocketArrayAR[counterPE].attackDamage;
								if (enemyArray[counterPE2].hp <= 0)
								{
									enemyArray[counterPE2].alive = false;
									planet.setMarketValue(planet.getMarketValue() + 10);
									scoreInt += 50;
									counterEMP++;
									if (counterEMP >= 20)
									{
										emp.setObtainedEMP(true);
										counterEMP = 0;
									}
								}
							}
							counterPE2++;
						}
						counterPE++;
					}
					//AI turret rocket collision with enemies
					counterPE = 0;
					for (iterTR = RocketArrayTR.begin(); iterTR != RocketArrayTR.end(); iterTR++)
					{
						counterPE2 = 0;
						for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
						{
							if (RocketArrayTR[counterPE].rectRocket.getGlobalBounds().intersects(enemyArray[counterPE2].enemyrect.getGlobalBounds()))
							{
								RocketArrayTR[counterPE].destroy = true;

								enemyArray[counterPE2].hp -= RocketArrayTR[counterPE].attackDamage;
								if (enemyArray[counterPE2].hp <= 0)
								{
									enemyArray[counterPE2].alive = false;
								}
							}
							counterPE2++;
						}
						counterPE++;
					}
					//planet collision with enemy
					counterE = 0;
					for (iterP = planetRectArray.begin(); iterP != planetRectArray.end(); iterP++)
					{
						counterPE = 0;
						for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
						{
							if (planetRectArray[counterE].getGlobalBounds().intersects(enemyArray[counterPE].enemyrect.getGlobalBounds()))
							{
								planet.setMarketValue(planet.getMarketValue() - 250);
								enemyArray[counterPE].alive = false;

							}
							/*else if (planet.rectNeder1.getGlobalBounds().intersects(enemyArray[counterPE].enemyrect.getGlobalBounds()))
							{
								planet.setMarketValue(planet.getMarketValue() - 250);
								enemyArray[counterPE].alive = false;
							}*/
							if (planet.getMarketValue() <= 0)
							{
								gameStatus = ENDSCREEN;
							}

							counterPE++;
						}
						counterE++;
					}
					// Delete Dead Enemy
					counterPE = 0;
					for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
					{
						if (enemyArray[counterPE].alive == false)
						{
							cout << "Enemy has been destroyed" << endl;
							enemyArray.erase(iter4);
							break;
						}

						counterPE++;
					}

					//Delete avatar projectile
					counterPE = 0;
					for (iterAR = RocketArrayAR.begin(); iterAR != RocketArrayAR.end(); iterAR++)
					{
						if (RocketArrayAR[counterPE].destroy == true)
						{
							RocketArrayAR.erase(iterAR);
							break;
						}

						counterPE++;
					}
					//Delete AIturret projectile
					counterPE = 0;
					for (iterAR = RocketArrayTR.begin(); iterAR != RocketArrayTR.end(); iterAR++)
					{
						if (RocketArrayTR[counterPE].destroy == true)
						{
							RocketArrayTR.erase(iterAR);
							break;
						}

						counterPE++;
					}

					// Draw Turret
					counterT = 0;
					for (iterT = TurretArray.begin(); iterT < TurretArray.end(); iterT++) {

						window.draw(TurretArray[counterT].spriteOver);
						window.draw(TurretArray[counterT].spriteNeder);
						counterT++;
					}


					//Draw Avatar Spirte overdel
					window.draw(avatar.spriteOver);
					// Pause
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && pressedL)
					{
						pressedL = false;
						gameStatus = PAUSED;
					}
					if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && !pressedL)
						pressedL = true;
				}
			}
			window.display();
		}
	}
}
