#include "EMP.h"

EMP::EMP()
{
	newObtainedEMP = false;
	newActivateEMP = false;
	newCX = 498; //498
	newCY = 298; // 398
	newCR = 5;

	//spriteEMP.setPosition(0,0);
	spriteEMP.setTextureRect(sf::IntRect(1019, 1098, 779, 404));
	circleEMP.setPosition(500, 400);
	circleEMP.setFillColor(sf::Color::Transparent);
	circleEMP.setRadius(5);
	circleEMP.setScale(2, 1); // �ndra activateFunktion()
	circleEMP.setOutlineColor(sf::Color::Magenta);
	circleEMP.setOutlineThickness(10);
	//circleEMP.setScale(10,10);



	/*setPointCount(100);
	setRadius(40);
	setOutlineColor
	setOutlineThickness
	setScale*/
}

void EMP::Activate(sf::RenderWindow &window)
{
	circleEMP.setPosition(newCX, newCY);
	//spriteEMP.setPosition(newCX, newCY);
	circleEMP.setRadius(newCR);
	newCX -= 10;
	newCY -= 5;
	newCR += 5;

	//if (newCR >= 22) // Effect 1     //(newCX <= 478 && newCY <= 298)
	//	spriteEMP.setTextureRect(sf::IntRect(347, 1649, 44, 41));
	//if (newCR >= 47) // Effect 2
	//	spriteEMP.setTextureRect(sf::IntRect(202, 1737, 94, 65));
	//if (newCR >= 72) // Effect 3
	//	spriteEMP.setTextureRect(sf::IntRect(202, 1649, 144, 87));
	//if (newCR >= 100) // Effect 4
	//	spriteEMP.setTextureRect(sf::IntRect(0, 1649, 201, 133));
	//if (newCR >= 174) // Effect 5
	//	spriteEMP.setTextureRect(sf::IntRect(1560, 1503, 348, 211));
	//if (newCR >= 540)//270) // Effect 6
	//	spriteEMP.setTextureRect(sf::IntRect(1019, 1503, 540, 310));
	//if (newCR >= 779)//389) // Effect 7
	//	spriteEMP.setTextureRect(sf::IntRect(1019, 1098, 779, 404));

	//if (newCR >= 47) // Effect 8
	//	spriteEMP.setTextureRect(sf::IntRect(202, 1737, 94, 65));
	//if (newCR >= 47) // Effect 9
	//	spriteEMP.setTextureRect(sf::IntRect(202, 1737, 94, 65));
	//if (newCR >= 47) // Effect 10
	//	spriteEMP.setTextureRect(sf::IntRect(202, 1737, 94, 65));

	//spriteEMP.setPosition(newCX,newCY);
	window.draw(circleEMP);
	//window.draw(spriteEMP);
}

bool EMP::getActivateEMP()
{
	return newActivateEMP;
}
bool EMP::getObtainedEMP()
{
	return newObtainedEMP;
}
float EMP::getCX()
{
	return newCX;
}
float EMP::getCY()
{
	return newCY;
}
float EMP::getCR()
{
	return newCR;
}

void EMP::setActivateEMP(bool EMPActivated)
{
	newActivateEMP = EMPActivated;
}
void EMP::setObtainedEMP(bool EMPObtained)
{
	newObtainedEMP = EMPObtained;
}

void EMP::setCX(float x)
{
	newCX = x;
}
void EMP::setCY(float y)
{
	newCY = y;
}
void EMP::setCR(float r)
{
	newCR = r;
}