#pragma once

#include "Incl.h"
#include "Avatar.h"
#include "Planet.h"

using namespace std;

class Rocket {
public:
	sf::Sprite spriteRocket;
	sf::RectangleShape rectRocket;
	sf::Vector2f mousePos;
	double movementSpeed = 7;
	float rotation = 0;
	float angle = 0;
	bool destroy = false;
	int attackDamage = 10;

	Rocket(sf::RenderWindow &window);
	void movementFromAvatar(sf::RenderWindow &window, Avatar avatar, Planet planet);

};